import { Button, Form, Row, Col} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom'; 

// sweetalert2 is a simple and usefule package for generating user alerts with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {
		// we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button
		if(email !== '' && password !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);

	// Allows us to consume the UserContext object and it's properties to use for validation
	const {user, setUser} = useContext(UserContext)	

	// Prev. Code (Comment out)
	// const [user, setUser] = useState(localStorage.getItem('email'));

	function loginUser(event) {
		// prevent page reloading
		event.preventDefault();

		// set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('property', value);
		// The localStorage.setItem() allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering
		// Because REACT JS is a single page application, using the localStorage will not trigger rerendering of component

		/*localStorage.setItem('email', email);

		setUser(localStorage.getItem('email'));
		alert('You are now logged in');
		navigate('/courses');

		setEmail('');
		setPassword('');*/

		// process fetch request to the corresponding backend API
		// Syntax:
			// fetch('url', {options}).then(response => response.json()).then(data => {});

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		            method: 'POST',
		            headers: {
		                'Content-Type' : 'application/json'
		            },
		            body: JSON.stringify({
		                email: email,
		                password: password
		            })
		        })
		.then(response => response.json())
		.then(data=> {
		    // console.log(data);

		   	// if statemen to check whether the login is successful
		   	if(data === false){
		   		/*alert('Login unsuccessful!');*/
		   		// in adding sweetalert2 you have to use the fire method
		   		Swal2.fire({
		   			title : 'Login unsuccessful!',
		   			icon : 'error',
		   			text: 'Check your login credentials and try again'
		   		})
		   	} else {
		   		localStorage.setItem('token', data.access);

		   		retrieveUserDetails(data.access);

		   		/*alert('Login succesful!')*/

		   		Swal2.fire({
		   			title : "Login successful!",
		   			icon : 'success',
		   			text : 'Welcome to Zuitt!'
		   		})

		   		navigate('/courses');
		   	}
		})
	}	

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization : `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id : data._id,
				isAdmin : data.isAdmin
			})
		})
	}

	return(	
		user.id === null || user.id === undefined
		?
			<Row>
				<Col className = "pt-2 col-6 mx-auto">
					<h1>Login</h1>
					<Form onSubmit = {event => loginUser(event)}>
					  <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control
					    	type="email"
					    	placeholder="Enter email"
					    	value = {email}
					    	onChange = {event => setEmail(event.target.value)}
					    />
					    {/*<Form.Text className="text-muted">
					      We'll never share your email with anyone else.
					    </Form.Text>*/}
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control
					    	type="password"
					    	placeholder="Password"
					    	value = {password}
					    	onChange = {event => setPassword(event.target.value)}
					    />
					  </Form.Group>

					  {/*<Form.Group className="mb-3" controlId="formBasicCheckbox">
					    <Form.Check type="checkbox" label="Check me out" />
					  </Form.Group>*/}

					  <Button variant="primary" type="submit" disabled = {isDisabled}>
					    Login
					  </Button>
					</Form>
				</Col>
			</Row>
		:
			<Navigate to = '/*' />
	);
}