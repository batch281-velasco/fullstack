const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (event) => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;
	let fullName = firstName + ' ' + lastName;

	spanFullName.innerHTML = fullName;
})

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);
