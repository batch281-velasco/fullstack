import { useState, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function CourseCard({ productProp }) {
  const { user } = useContext(UserContext);
  const { name, description, price, _id, image } = productProp;

  return (
        <Card className=" mx-auto" style={{ width: "20%" }}>
          <Card.Img variant="top" class ="img-fluid" src={image} alt={name} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            {/*<Card.Text>{description}</Card.Text>*/}
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>P{price}</Card.Text>
            {user._id !== null ? (
              <>
                <div class="mt-2 text-center">
                  <Link className="btn btn-success" to={`/productView/${_id}`}>
                    Details
                  </Link>
                </div>
              </>
            ) : (
              <>
                <div class="mt-2 text-center">
                  <Link className="btn btn-primary" to={`/login`}>
                    Details
                  </Link>
                </div>
              </>
            )}
          </Card.Body>
        </Card>
  );
}
