import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import "../App.css";
export default function Home() {
  return (
    <>
      <div id="wholebg">
        <Banner />
      </div>
      {/*<div id="btn">
        <Button
          className="shopNow"
          id="btn"
          variant="success"
          as={Link}
          to="/products"
        >
          SHOP NOW!
        </Button>
      </div>*/}
      <Highlights />
    </>
  );
}
