import {Row, Col, Button,} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Banner(){
  return(

    <Row>
      <Col className = "p-5">
        <h1>404 -Page Not Found</h1>
        <p>The page you are looking for not found</p>
        <Button variant="primary" as={Link} to="/">Back to Home</Button>
      </Col>
    </Row>

  )
}