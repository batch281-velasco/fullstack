let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    
    collection[collection.length] = element;
    return collection;

}

function dequeue() {
    // In here you are going to remove the first element in the array

    /*let newCollection = [];

    for(let i = 0; i > collection.length; i++){
        // newCollection[i] = collection[i];
        newCollection = collection[i];    
    }

    // return newCollection;
    return [...new Set(newCollection)];*/

    /*let newCollection = [];

    let i = 0;
    while(collection[i] !== undefined){
        i++;
    }

    let j = 0;
    while(collection[j] !== undefined){
        j++;
    }

    for( let k = 0; k < i; k++){
        if(collection[k] == collection[0]){
            newCollection[j] = collection[k];
            collection = newCollection;
        }
    }
    return newCollection;
*/
    let i = 1;
    collection[i-1] = collection[i];
    collection.length = collection.length - i;            
    return collection;
}

function front() {
    // you will get the first element

    // return collection[0]
    i = collection.length - collection.length;

    return collection[i];
}

// Starting from here, array.length should not be used

function size() {
     // Number of elements

    let size = 0;

    while(collection[size] !== undefined){
        size++;
    }

    return size;
}

function isEmpty() {
    //it will check whether the function is empty or not

    let size = 0;

    while(collection[size] !== undefined){
        size++;
    }

    // return size;

    if(size === 0){
        return true;
    } else {
        return false;
    }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};